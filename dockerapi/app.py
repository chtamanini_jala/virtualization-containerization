import time
import socket
from flask import Flask, request, jsonify
from datetime import datetime

app = Flask(__name__)

data = [
   {
      'hostname': socket.gethostname(),
      'IP Address': socket.gethostbyname(socket.gethostname()),
      'Time': datetime.now().time().strftime('%H:%M:%S')
   }
]

app.route('/', methods=['GET'])
def hello():
   return jsonify(data)